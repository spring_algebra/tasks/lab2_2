/*
 * Algebra labs.
 */

package com.example.demo.domain;

import org.junit.Test;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.example.demo.config.SpringConfig;
import com.example.demo.service.Catalog;

public class UT_Catalog {
	@Test
	public void catalogTest() {
            
                //TODO: get context from configuration class 
		

		Catalog catalog = ctx.getBean(Catalog.class);
		
		for (MusicItem musicItem : catalog.findByKeyword("a")) {
			System.out.println(musicItem);
		}
		
		ctx.close();
	}

}
